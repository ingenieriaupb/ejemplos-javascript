var btns;
var secciones;
var usuario_txt;
var score;
var tabla_posiciones;

window.onload = init;

function init(){
	btns = document.getElementsByClassName("boton");
	secciones = document.getElementsByTagName("section");
	usuario_txt = document.getElementById("usuario");
	tabla_posiciones = document.getElementById("tabla_posiciones");

	asignarEventos();
	cargarTop();
}
function pintarScore()
{
	var tabla = '<table class="table table-striped">';
	tabla += '<tr><th>Nombre</th><th>Puntaje</th><th>Posición</th></tr>';	
	for(var i in score){
		//console.log(score[i]);
		tabla+= '<tr><td>'+score[i].nombre+'</td>';
		tabla+= '<td>'+score[i].puntaje+'</td>';
		tabla+= '<td>'+i+'</td></tr>';
	}
	tabla+= '</table>';
	tabla_posiciones.innerHTML = tabla;
}
function cargarTop()
{
	score = JSON.parse(localStorage.getItem("score"));
	
	if(score)
	{
		pintarScore();
	}
	else
	{
		score = [];
	}
}
function actualizarPuntaje(){
	var usuario = {};
	usuario.nombre = usuario_txt.value;
	usuario.puntaje = Math.round(Math.random()*10000);
	score.push(usuario);
	localStorage.setItem("score",JSON.stringify(score));
	usuario_txt.value = "";	
	pintarScore();
}
function asignarEventos()
{
	var btn_guardar = document.getElementById("btn_guardar");
	btn_guardar.addEventListener("click",actualizarPuntaje);

	for(var i=0;i<btns.length;i++)
	{
		btns[i].addEventListener("click",cambiarSeccion.bind({i:i}));
	}
}
function ocultarSeccion(){
	for(var i=0;i<btns.length;i++)
	{
		secciones[i].className = "hidden";
	}
}
function cambiarSeccion(event){
	console.log(this.i);
	ocultarSeccion();
	secciones[this.i].className = "active show animated fadeInLeft";
}